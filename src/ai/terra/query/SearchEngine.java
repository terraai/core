package ai.terra.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.RAMDirectory;

public abstract class SearchEngine<T extends Searchable> {
	private RAMDirectory index;
	private StandardAnalyzer analyzer;

	public SearchEngine() {
	}
	
	public synchronized void reindex() {
		index = new RAMDirectory();
		analyzer = new StandardAnalyzer();
		
		IndexWriterConfig config = new IndexWriterConfig(analyzer);
		
		try (IndexWriter writer =  new IndexWriter(index, config)) {
			List<T> l = new ArrayList<T>();
			l.addAll(getData());
			
			// Add documents
			for (Searchable s : l) {
				writer.addDocument(s.getSearchableDocument());
			}
		} catch (Exception ioe) {
			ioe.printStackTrace();
		}
	}
	
	public abstract Collection<T> getData() throws Exception;

	public synchronized List<Searchable> query(String q) throws Exception {
		// Build an IndexSearcher using the in-memory index
		IndexSearcher searcher = new IndexSearcher(DirectoryReader.open(index));
		
		Query query = new QueryParser("title", analyzer).parse(q);
		
		List<Searchable> results = new ArrayList<Searchable>();
		
		int count = searcher.count(query);
		if (count <= 0) return results;
		
		// Search for the query, returning all matches
		TopDocs hits = searcher.search(query, count);
		
		// Examine the Hits object to see if there were any matches
		for (int i = 0; i < hits.scoreDocs.length; i++) {
			Document doc = searcher.doc(hits.scoreDocs[i].doc);
			results.add(getSearchableForDocument(doc));
		}
		
		return results;
	}
	
	public abstract T getSearchableForDocument(Document d) throws Exception;
}
