package ai.terra.query;

import org.apache.lucene.document.Document;

public interface Searchable {
	Document getSearchableDocument();
}
