package ai.terra.query;

import java.util.List;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexableField;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * TODO  Automatically populate fields based on JSON.
 */
public abstract class DefaultSearchable implements Searchable {
	public @JsonIgnore Document getSearchableDocument() {
		Document d = new Document();
		for (IndexableField f : getSearchableFields()) d.add(f);
		return d;
	}
	
	public @JsonIgnore abstract List<IndexableField> getSearchableFields();
}
