package ai.terra.s3;

import com.terra_ai.model.geo.State;
import com.terra_ai.model.wells.WellHead;

public class DocumentsQueries {
	public void populateDocuments(WellHead h, boolean refresh) {
		if (!h.in(State.CA)) return; // Currently we only have logs for california
		
		try {
			if (refresh || !h.hasDocuments()) {
				h.clearDocuments();
				h.addDocuments(new Bucket("los-angeles-logsall").find(h.getAPI().withoutStateCode()));
			}
		} catch (Throwable t) {
			t.printStackTrace();
		}
	}
}
