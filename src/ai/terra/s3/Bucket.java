package ai.terra.s3;

import java.util.ArrayList;
import java.util.List;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.ListObjectsV2Request;
import com.amazonaws.services.s3.model.ListObjectsV2Result;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import ai.terra.aws.AWSCredentials;
import com.terra_ai.model.Document;

public class Bucket {
	private String name;

	public Bucket(String name) { this.name = name; }
	
	public List<Document> find(String prefix) {
		List<Document> docs = new ArrayList<Document>();
		
		AmazonS3 s3client = new AmazonS3Client(AWSCredentials.getProvider());
		
		try {
			ListObjectsV2Request req = new ListObjectsV2Request().withBucketName(name).withMaxKeys(2);
			ListObjectsV2Result result;
			
			do {
				result = s3client.listObjectsV2(req);
				
				for (S3ObjectSummary objectSummary :  result.getObjectSummaries()) {
					if (objectSummary.getKey().startsWith(prefix)) {
						docs.add(new Document(objectSummary));
					}
				}
				
				req.setContinuationToken(result.getNextContinuationToken());
			} while (result.isTruncated() == true);
		} catch (AmazonServiceException ase) {
			System.out.println("Error Message:    " + ase.getMessage());
			System.out.println("HTTP Status Code: " + ase.getStatusCode());
			System.out.println("AWS Error Code:   " + ase.getErrorCode());
			System.out.println("Error Type:       " + ase.getErrorType());
			System.out.println("Request ID:       " + ase.getRequestId());
		} catch (AmazonClientException ace) {
			System.out.println("Error Message: " + ace.getMessage());
		}
		
		return docs;
	}
}
