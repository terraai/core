package ai.terra.cog;

import ai.terra.person.Identity;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.services.cognitoidp.AWSCognitoIdentityProvider;
import com.amazonaws.services.cognitoidp.model.*;
import io.flowtree.aws.CognitoLogin;
import io.flowtree.aws.Encryptor;

public class Cognition extends CognitoLogin implements Identity {
	private AWSCognitoIdentityProvider c;
	private AWSCredentialsProvider p;

	public Cognition(Encryptor e, AWSCredentialsProvider p) {
		super(e, p);
	}

	public void listUsers() {
		System.out.println("User pools: ");
		ListUserPoolsRequest req = new ListUserPoolsRequest();
		req.setMaxResults(10);
		ListUserPoolsResult r = c.listUserPools(req);

		for (UserPoolDescriptionType s : r.getUserPools()) {
			System.out.println("\t" + s.getName());

			ListUsersRequest lr = new ListUsersRequest();
			lr.setLimit(60);
			lr.setUserPoolId(s.getId());

			ListUsersResult res = c.listUsers(lr);
			for (UserType u : res.getUsers()) {
				System.out.println("\t    " + u.getUsername() + " -- " + u.getUserStatus() +
						" (" + u.getUserLastModifiedDate() + ")");
			}
		}
	}
}
