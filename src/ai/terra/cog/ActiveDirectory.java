package ai.terra.cog;

import ai.terra.crypto.Encryptor;
import ai.terra.person.Identity;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.services.clouddirectory.AmazonCloudDirectory;
import com.amazonaws.services.clouddirectory.AmazonCloudDirectoryClientBuilder;
import com.amazonaws.services.directory.model.CreateComputerRequest;

public class ActiveDirectory implements Identity {
	private AmazonCloudDirectory d;

	public ActiveDirectory(Encryptor e, AWSCredentialsProvider p) {
		d = AmazonCloudDirectoryClientBuilder.standard().withRegion(e.getRegion())
											.withCredentials(p).build();
		CreateComputerRequest request = new CreateComputerRequest();
		request.setComputerName("Terra Ai's First computer");
		request.setDirectoryId("group1");
//		d.createObject(new CreateObjectRequest()
//							.withSchemaFacets(new SchemaFacet()
//													.withFacetName("member")
//													.withSchemaArn("arn:aws:clouddirectory:user/ashesfall"))
//							.withDirectoryArn("arn:aws:clouddirectory:orgs"));
	}

	@Override
	public void listUsers() {

	}

	@Override
	public boolean checkPassword(String user, String password) {
		return false;
	}
}
