package ai.terra.aws;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.ClasspathPropertiesFileCredentialsProvider;

public class AWSCredentials {
	public static AWSCredentialsProvider getProvider() {
//		return new EnvironmentVariableCredentialsProvider();
		return new ClasspathPropertiesFileCredentialsProvider("/ai/terra/aws/AwsCredentials.properties");
//		return new ProfileCredentialsProvider("Terra AI");
//		return new ProfileCredentialsProvider();
	}
}
