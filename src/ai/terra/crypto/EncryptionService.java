package ai.terra.crypto;

import ai.terra.cog.ActiveDirectory;
import ai.terra.cog.Cognition;
import ai.terra.person.Identity;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;

import java.security.NoSuchAlgorithmException;

public class EncryptionService {
    private static Encryptor e;

    private Identity id;

    public EncryptionService(String myAccessKeyId, String mySecretKey, String idKeyId, String idSecretKey, boolean useCognito) throws NoSuchAlgorithmException {
        e = new Encryptor(myAccessKeyId, mySecretKey);

        final BasicAWSCredentials c = new BasicAWSCredentials(idKeyId, idSecretKey);

        if (useCognito) {
            this.id = new Cognition(e, new AWSCredentialsProvider() {
                @Override
                public AWSCredentials getCredentials() {
                    return c;
                }

                @Override
                public void refresh() { }
            });
        } else {
            this.id = new ActiveDirectory(e, new AWSCredentialsProvider() {
                @Override
                public AWSCredentials getCredentials() {
                    return c;
                }

                @Override
                public void refresh() { }
            });
        }

        this.id.listUsers();
    }

    public Identity getId() { return this.id; }

    public static void main(String bucket, String key, String file) throws Exception {
        e.doCrypt(bucket, key, file);
    }
}
