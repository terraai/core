package ai.terra.google;

import java.net.MalformedURLException;
import java.net.URL;

import org.almostrealism.texture.ImageLayers;
import org.almostrealism.texture.ImageSource;
import org.almostrealism.texture.URLImageSource;
import com.terra_ai.model.geo.LatLong;
import com.terra_ai.model.geo.Point;

public class MapsImageProvider {
	public MapsImageProvider() { }
	
	public ImageLayers getImage(Point p) throws MalformedURLException {
		LatLong l = p.getCoordinates();
		
		ImageSource sat = new URLImageSource(
							new URL("https://maps.googleapis.com/maps/api/staticmap?maptype=satellite&center=" +
									l.getLatitude() + "," + l.getLongitude() +
									"&zoom=14&size=800x800&key=" + GoogleClient.API_KEY));
		
		ImageLayers layers = new ImageLayers();
		layers.addLayer("satellite", sat);
		
		return layers;
	}
}
