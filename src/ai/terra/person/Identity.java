package ai.terra.person;

import io.almostrealism.auth.Login;

public interface Identity extends Login {
	void listUsers();
}
