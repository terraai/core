package ai.terra.persistence.etl;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.terra_ai.model.wells.WellHead;

import io.almostrealism.etl.View;
import io.almostrealism.sql.SQLConnectionProvider;

public class WellHeadByOperatorView extends View<WellHead> {

	public WellHeadByOperatorView(SQLConnectionProvider c, Collection<WellHead> values) {
		super(c, "superset.well_head_by_operator", values);
	}

	@Override
	public Map<String, String> encode(WellHead value) {
		Map<String, String> data = new HashMap<String, String>();
		
		data.put("api", value.getAPI().toString());
		data.put("operator_name", quote(value.getOperator()));
		data.put("cum_oil", String.valueOf(value.getCumulativeOil()));
		
		return data;
	}

	@Override
	public String[] getPrimaryKeys() { return new String[] { "api" }; }
}
