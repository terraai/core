package ai.terra.persistence.etl;

import java.util.HashMap;
import java.util.Map;

import com.terra_ai.model.geo.Pool;
import com.terra_ai.model.wells.WellHead;

import io.almostrealism.etl.View;
import io.almostrealism.sql.SQLConnectionProvider;

public class WellHeadByPoolView extends View<Map.Entry<Pool, WellHead>> {

	public WellHeadByPoolView(SQLConnectionProvider c, Map<Pool, WellHead> values) {
		super(c, "superset.well_head_by_pool", values.entrySet());
	}

	@Override
	public Map<String, String> encode(Map.Entry<Pool, WellHead> value) {
		Map<String, String> data = new HashMap<String, String>();
		
		data.put("api", value.getValue().getAPI().toString());
		data.put("pool_name", value.getKey() == null ? null : quote(value.getKey().getName()));
		data.put("cum_oil", String.valueOf(value.getValue().getCumulativeOil(value.getKey())));
		
		return data;
	}

	@Override
	public String[] getPrimaryKeys() { return new String[] { "api", "pool_name" }; }
}
