package ai.terra.persistence.test;

import org.junit.Test;

import com.terra_ai.model.forecast.FFRForecastMetaData;
import ai.terra.persistence.Sources;

public class SourcesTest {
	@Test
	public void ffrMetadataTest() throws Exception {
		FFRForecastMetaData data = new FFRForecastMetaData();
		data.setNotes("This is a test meta data entry");
		Sources.ForecastMetadata.insert("dynamodb", data);
	}
}
