package ai.terra.sql.old;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.terra_ai.model.business.Group;
import com.terra_ai.model.business.GroupList;
import ai.terra.sql.Database;

public class BusinessQueries extends Database {
	private WellQueries prod;
	
	private GroupList groups;
	
	public BusinessQueries(Connection c, WellQueries q) { super(c); this.prod = q; }
	
	public synchronized GroupList getGroups() throws SQLException {
		if (groups == null) {
			groups = new GroupList();
			
			try (Statement s = connection.createStatement()) {
				ResultSet rs = s.executeQuery("select distinct(Field_Name) from cali_la_wells");
				System.out.println("Geo query has returned");
				
				while (rs.next()) {
					groups.add(new Group(rs.getString(1)));
				}
			}
		}
		
		return groups;
	}
	
	public synchronized Group getGroup(String name) throws SQLException {
		GroupList l = getGroups();
		
		for (Group g : l) {
			if (name.equals(g.getName())) {
				prod.populateGroup(g);
				return g;
			}
		}
		
		return null;
	}
}
