package ai.terra.sql.old;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import ai.terra.google.GoogleClient;
import com.terra_ai.model.geo.LatLong;
import com.terra_ai.model.geo.Point;
import com.terra_ai.model.wells.API_10;
import com.terra_ai.model.wells.WellBore;
import com.terra_ai.model.wells.WellHead;
import ai.terra.sql.Database;

/**
 * Loads geo data and well type data for the top level of the {@link WellBore} data object.
 * 
 * @author  Michael Murray
 */
public class GeoQueries extends Database {
	public static final String geoQueryPre = "https://maps.googleapis.com/maps/api/elevation/json?locations=";
	public static final String geoQueryPost = "&key=" + GoogleClient.API_KEY;
	
	public GeoQueries(Connection c) { super(c); }
	
	public void updateWellLocations(Map<String, WellHead> wells) throws SQLException {
		List<API_10> unknownLocations = new ArrayList<API_10>();
		for (WellHead w : wells.values()) {
			if (!w.hasGeo()) unknownLocations.add(w.getAPI());
		}
		
		try (Statement s = connection.createStatement()) {
			ResultSet rs = s.executeQuery("select * from master_tableau where API_master in (" +
											getKeys(unknownLocations) + ")");
			
			while (rs.next()) {
				WellHead w = wells.get(rs.getString("API_master"));
				
				LatLong location = new LatLong(rs.getDouble("lat"), rs.getDouble("longi"));
				w.setLocation(new Point(location));
				w.setWellType(rs.getString("welltype"));
			}
		}
	}
	
	private String getKeys(Iterable<API_10> keys) {
		StringBuffer buf = new StringBuffer();
		
		for (API_10 s : keys) {
			buf.append(s.getAPI10());
			buf.append(",");
		}
		
		return buf.toString().substring(0, buf.length() - 1);
	}
}
