package ai.terra.sql.old;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;
import java.util.Hashtable;

import com.terra_ai.model.business.Group;
import com.terra_ai.model.geo.LatLong;
import com.terra_ai.model.geo.Point;
import com.terra_ai.model.wells.API_10;
import com.terra_ai.model.wells.WellBore;
import com.terra_ai.model.wells.WellHead;
import ai.terra.s3.DocumentsQueries;
import ai.terra.sql.Database;
import ai.terra.sql.results.GPResultsQueries;
import ai.terra.sql.results.PhysicalModelingResultsQueries;
import ai.terra.sql.tagging.CFTaggingQueries;

/**
 * Query engine for big_prod_pool table.
 * 
 * @author  Michael Murray
 */
public class WellQueries extends Database {
	private GeoQueries geo;
	private GPResultsQueries gpResults;
	private PhysicalModelingResultsQueries pmResults;
	private CFTaggingQueries cfTags;
	
	private DocumentsQueries docs;
	
	private Hashtable<API_10, WellHead> wellHeads;
	
	public WellQueries(Connection c) {
		super(c);
		geo = new GeoQueries(c);
		gpResults = new GPResultsQueries(c);
		pmResults = new PhysicalModelingResultsQueries(c);
		cfTags = new CFTaggingQueries(c);
		docs = new DocumentsQueries();
	}
	
	public Collection<WellHead> allWells() throws SQLException {
		if (wellHeads == null) {
			wellHeads = new Hashtable<API_10, WellHead>();
			
			for (WellHead h : wellHeads.values()) {
				for (WellBore b : h.getBores()) {
					updateService.submit(() -> {
						try (Statement s = connection.createStatement()) {
							ResultSet rs = s.executeQuery("select count(prod_days) from big_prod_pool where API_prod = " + b.getAPI().getAPI12());
							
							if (rs.next()) {
								b.setProdDaysAvailable(rs.getInt(1));
							}
						} catch (SQLException e) {
							e.printStackTrace();
						}
						
						/*
						JsonNode n = null;
						try {
							Thread.sleep(100);
							n = mapper.readValue(new URL(GeoQueries.geoQueryPre + w.getWellHead().getLatitude() + "," + w.getWellHead().getLongitude()), JsonNode.class);
							w.getWellHead().setElevation(Double.parseDouble(n.get("results").get(0).get("elevation").toString()));
						} catch (Exception ex) {
							System.out.println(n);
						}
						*/
						
						try {
							cfTags.updateTags(b);
						} catch (Exception e) {
							e.printStackTrace();
						}
					});
				}
			}
			
			System.out.println("There are " + wellHeads.values().size() + " wells");
		}
		
		return wellHeads.values();
	}
	
	public synchronized void populateGroup(Group g) throws SQLException {
		try (Statement s = connection.createStatement()) {
			ResultSet rs = s.executeQuery("SELECT * from big_master_detail where field_name = \"" + g.getName() + "\"");
			
			while (rs.next()) {
				WellHead h = getWellHead(new API_10(rs.getString("API_master")));
				h.setLocation(new Point(new LatLong(rs.getDouble("lat"), rs.getDouble("longi"))));
				h.setWellType(rs.getString("well_type"));
				g.addWell(h);
				
				try {
					docs.populateDocuments(h, false);
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public synchronized API_10 randomWell() throws SQLException {
		try (Statement s = connection.createStatement()) {
			ResultSet rs = s.executeQuery("SELECT API_master FROM NDK_master where API_master ORDER BY RAND() LIMIT 1");
			rs.next();
			return new API_10(rs.getString("API_master"));
		}
	}
	
	private synchronized WellHead getWellHead(API_10 api) {
		if (wellHeads == null) wellHeads = new Hashtable<API_10, WellHead>();
		
		if (!wellHeads.containsKey(api)) {
			wellHeads.put(api, new WellHead(api));
		}
		
		return wellHeads.get(api);
	}
}
