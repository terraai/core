package ai.terra.sql;

import com.terra_ai.model.business.Group;
import com.terra_ai.model.forecast.OilProductionForecast;
import com.terra_ai.model.geo.Field;
import com.terra_ai.model.well.completion.Completion;
import com.terra_ai.model.wells.API_2;
import com.terra_ai.model.wells.ProdDataEntry;
import com.terra_ai.model.wells.WellBore;
import com.terra_ai.model.wells.WellHead;
import ai.terra.sql.publicdata.CompletionQuery;
import ai.terra.sql.publicdata.FieldQuery;
import ai.terra.sql.publicdata.ProdDataEntryQuery;
import ai.terra.sql.publicdata.WellBoreQuery;
import ai.terra.sql.publicdata.WellHeadQuery;
import ai.terra.sql.results.NDKResultsQuery;
import ai.terra.sql.results.UtahResultsQuery;

import io.almostrealism.query.QueryLibrary;

public final class TerraQueryLibrary {
	private static boolean initialized;
	
	private TerraQueryLibrary() { }
	
	public static void initialize() {
		if (initialized) return;
		QueryLibrary.root().addQuery(Field.class, API_2.class, new FieldQuery());
		QueryLibrary.root().addQuery(WellHead.class, Field.class, new WellHeadQuery());
		QueryLibrary.root().addQuery(WellBore.class, WellHead.class, new WellBoreQuery());
		QueryLibrary.root().addQuery(ProdDataEntry.class, WellHead.class, new ProdDataEntryQuery());
		QueryLibrary.root().addQuery(Completion.class, WellBore.class, new CompletionQuery());
		QueryLibrary.root().addQuery(OilProductionForecast.class, Group.class, new NDKResultsQuery());
		QueryLibrary.root().addQuery(OilProductionForecast.class, Group.class, new UtahResultsQuery());
		initialized = true;
	}
}
