package ai.terra.sql.publicdata;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.terra_ai.model.geo.Field;
import com.terra_ai.model.geo.LatLong;
import com.terra_ai.model.geo.Point;
import com.terra_ai.model.wells.API_10;
import com.terra_ai.model.wells.WellHead;
import ai.terra.sql.PublicDataSourceSQL;

import io.almostrealism.persist.CascadingQuery;

public class WellHeadQuery extends CascadingQuery<PublicDataSourceSQL, Field, WellHead> {
	@Override
	public boolean isRoot() { return true; }
	
	@Override
	public void init(Field field) { field.getWells().clear(); }

	@Override
	public String getQuery(Field field) {
		if (field.getName() == null) {
			return "SELECT * from public_data.big_master_detail,public_data.big_prod_sum where " +
					"big_master_detail.\"API_master\" = big_prod_sum.\"API_prod\" and field_name = null" +
					" and \"state_API_master\"=" + field.getState();
		} else {
			return "SELECT * from public_data.big_master_detail,public_data.big_prod_sum where " +
					"big_master_detail.\"API_master\" = big_prod_sum.\"API_prod\" and field_name = '" +
					field.getName().replaceAll("'", "''") + "' and \"state_API_master\"=" + field.getState();
		}
	}

	@Override
	public Collection<WellHead> getReturnValue(Field key) {
		return key.getWells();
	}

	@Override
	public WellHead process(ResultSet rs, Field field, Map<Class, List<CascadingQuery>> cascades) throws SQLException {
		String api = rs.getString("API_master");
		API_10 api10 = new API_10(api);
		
		WellHead h = field.getWell(api10);
		if (h == null) {
			h = new WellHead(api10);
			h.setWellType(rs.getString("well_type"));
			h.setWellStatus(rs.getString("well_status"));
			h.setLocation(new Point(new LatLong(rs.getDouble("lat"), rs.getDouble("longi"))));
			h.setOperator(rs.getString("operator_name"));
			h.setCounty(rs.getString("county"));
			field.getWells().add(h);
		}
		
		processCascades(rs, h, cascades);
		return h;
	}
}
