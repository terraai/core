package ai.terra.sql.publicdata;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.terra_ai.model.geo.Pool;
import com.terra_ai.model.well.completion.Completion;
import com.terra_ai.model.wells.API_14;
import com.terra_ai.model.wells.WellBore;
import ai.terra.sql.PublicDataSourceSQL;

import io.almostrealism.persist.CascadingQuery;

public class CompletionQuery extends CascadingQuery<PublicDataSourceSQL, WellBore, Completion> {
	@Override
	public void init(WellBore bore) {
		bore.getCompletions().clear();
	}
	
	@Override
	public String getQuery(WellBore key) {
		return "SELECT * from public_data.big_master_detail where \"API_master_string\" LIKE '" + key.getAPI() + "%'";
	}

	@Override
	public Collection<Completion> getReturnValue(WellBore bore) {
		return bore.getCompletions();
	}

	@Override
	public Completion process(ResultSet rs, WellBore bore, Map<Class, List<CascadingQuery>> cascades) throws SQLException {
		API_14 api = new API_14(rs.getString("API_master_string"));
		
		if (bore.getCompletion(api) == null) {
			Completion c = new Completion(api);
			c.setPool(new Pool(rs.getString("pool_name_master")));
			bore.getCompletions().add(c);
		}
		
		Completion c = bore.getCompletion(api);
		processCascades(rs, c, cascades);
		return c;
	}
}
