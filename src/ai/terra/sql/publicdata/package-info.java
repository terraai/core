/**
 * SQL for accessing public data.
 * 
 * @author  Michael Murray
 */
package ai.terra.sql.publicdata;