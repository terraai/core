package ai.terra.sql.publicdata;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.terra_ai.model.geo.Field;
import com.terra_ai.model.wells.API_2;
import ai.terra.sql.PublicDataSourceSQL;

import io.almostrealism.persist.CascadingQuery;

public class FieldQuery extends CascadingQuery<PublicDataSourceSQL, API_2, Field> {
	private Map<API_2, List<Field>> fields = new HashMap<API_2, List<Field>>();
	
	public boolean isRoot() { return true; }
	
	@Override
	public void init(API_2 key) { }

	@Override
	public String getQuery(API_2 key) {
		return "SELECT distinct(field_name) from public_data.big_master_detail " +
				"where \"state_API_master\" = " + key.getStateCode();
	}

	@Override
	public Collection<Field> getReturnValue(API_2 api) {
		return fields.containsKey(api) ? fields.get(api) : new ArrayList<Field>();
	}

	@Override
	public synchronized Field process(ResultSet rs, API_2 api, Map<Class, List<CascadingQuery>> cascades) throws SQLException {
		if (!fields.containsKey(api)) fields.put(api, new ArrayList<Field>());
		
		Field f = new Field(api, rs.getString("field_name"));
		List<Field> l = fields.get(api);
		if (!l.contains(f)) l.add(f);
		processCascades(rs, f, cascades);
		return f;
	}
}
