package ai.terra.sql.publicdata;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.terra_ai.model.wells.API_12;
import com.terra_ai.model.wells.WellBore;
import com.terra_ai.model.wells.WellHead;
import ai.terra.sql.PublicDataSourceSQL;

import io.almostrealism.persist.CascadingQuery;

public class WellBoreQuery extends CascadingQuery<PublicDataSourceSQL, WellHead, WellBore> {
	@Override
	public void init(WellHead key) {
		key.getBores().clear();
	}
	
	@Override
	public String getQuery(WellHead key) {
		return "SELECT * from public_data.big_master_detail where \"API_master_string\" LIKE '" + key.getAPI() + "%'";
	}

	@Override
	public Collection<WellBore> getReturnValue(WellHead key) {
		return key.getBores();
	}

	@Override
	public WellBore process(ResultSet rs, WellHead well, Map<Class, List<CascadingQuery>> cascades) throws SQLException {
		String apiString = rs.getString("API_master_string");
		if (apiString == null) return null;
		API_12 api = new API_12(apiString);
		
		if (well.getBore(api) == null) {
			WellBore b = new WellBore(api);
			b.setWellboreType(rs.getString("wellbore_type"));
			b.setSpudDate(rs.getString("spud_date"));
			well.getBores().add(b);
		}

		WellBore b = well.getBore(api);
		processCascades(rs, b, cascades);
		return b;
	}
}
