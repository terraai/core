package ai.terra.sql.publicdata;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.terra_ai.model.geo.Pool;
import com.terra_ai.model.wells.ProdDataEntry;
import com.terra_ai.model.wells.WellHead;
import ai.terra.sql.PublicDataSourceSQL;

import io.almostrealism.persist.CascadingQuery;

public class ProdDataEntryQuery extends CascadingQuery<PublicDataSourceSQL, WellHead, ProdDataEntry> {
	@Override
	public void init(WellHead h) {
		h.getReports().clear();
	}
	
	@Override
	public String getQuery(WellHead h) {
		h.getReports().clear();
		return "SELECT * from public_data.big_prod_sum where \"API_prod\" = " +
				h.getAPI().getAPI10() + " or \"API_prod\" = " + h.getAPI().getAPI12() +
				" or \"API_prod\" = " + h.getAPI().getAPI14();
	}
	
	public Collection<ProdDataEntry> getReturnValue(WellHead h) {
		return h.getReports();
	}
	
	@Override
	public ProdDataEntry process(ResultSet rs, WellHead key, Map<Class, List<CascadingQuery>> cascades) throws SQLException {
		String reportdate = rs.getString("report_date_mo");
		
		if (key.getReport(reportdate) == null) {
			key.getReports().add(new ProdDataEntry(
					reportdate,
					rs.getString("oil_mo"),
					rs.getString("gas_mo"),
					rs.getString("water_mo"),
					rs.getString("prod_days"),
					new Pool(rs.getString("pool_name"))));
		}
		
		ProdDataEntry d = key.getReport(reportdate);
		processCascades(rs, d, cascades);
		return d;
	}
}
