package ai.terra.sql;

import java.sql.Connection;

import io.almostrealism.persist.EntityUpdateService;

public class Database {
	protected Connection connection;

	protected EntityUpdateService updateService;
	
	public Database(Connection c) { connection = c; updateService = new EntityUpdateService(); }
}
