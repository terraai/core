package ai.terra.sql.tagging;

import java.sql.Connection;
import java.sql.SQLException;

import com.terra_ai.model.wells.WellBore;
import ai.terra.sql.Database;

public abstract class TaggingQueries extends Database {
	public TaggingQueries(Connection c) { super(c); }
	
	public abstract void updateTags(WellBore w) throws SQLException;
}
