package ai.terra.sql.tagging;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.terra_ai.model.wells.WellBore;
import com.terra_ai.model.wells.tags.Tag;

public class CFTaggingQueries extends TaggingQueries {
	public CFTaggingQueries(Connection c) { super(c); }

	public void updateTags(WellBore w) throws SQLException {
		try (Statement s = connection.createStatement()) {
			ResultSet rs = s.executeQuery("select * from results.CFTags_ND_SQLBased_2 where API = " + w.getAPI().getAPI12());
			
			if (rs.next()) {
				w.setRunLife(rs.getInt("RunLife"));
				w.setTag("CFTags_ND_SQLBased_2", new Tag(rs.getString("StartDate"),
														rs.getString("EndDate"),
														"CF", rs.getString("CF_flag")));
			}
		}
	}
}
