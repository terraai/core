package ai.terra.sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import io.almostrealism.sql.SQLConnectionProvider;

public class PublicDataSourceSQL implements SQLConnectionProvider {

	@Override
	public Connection getSQLConnection() throws SQLException {
		try {
			Class.forName("org.postgresql.Driver");
		}  catch (ClassNotFoundException e) {
		    e.printStackTrace();
		}
		
		return DriverManager.getConnection("jdbc:postgresql://terra.c4lstuf6msdr.us-west-2.rds.amazonaws.com/terra",
											"terra", "fundus500k");
	}
}
