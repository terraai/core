package ai.terra.sql.results;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import com.terra_ai.model.business.Group;
import com.terra_ai.model.forecast.OilProduction;
import com.terra_ai.model.forecast.OilProductionForecast;
import com.terra_ai.model.well.completion.Completion;
import com.terra_ai.model.wells.API_14;

import io.almostrealism.persist.CascadingQuery;

public abstract class API14ResultsQuery extends AggregatedResultsQuery {
	@Override
	public String getQuery(Group g) {
		StringBuffer buf = new StringBuffer();
		
		for (Completion c : g.getCompletions()) {
			buf.append(c.getAPI());
			buf.append(",");
		}
		
		if (buf.length() <= 0) return null;
		
		return "SELECT * from results.\"" + getName() + "\" where \"API_prod\" in (" + buf.substring(0, buf.length() - 1) + ")";
	}
	
	@Override
	public OilProductionForecast process(ResultSet rs, Group g, Map<Class, List<CascadingQuery>> cascades) throws SQLException {
		Completion c = g.getCompletion(new API_14(rs.getString("API_prod")));
		
		OilProductionForecast p = c.getOilForecasts().get(getName());
		if (p == null) {
			p = new OilProductionForecast(getName());
			c.getOilForecasts().put(getName(), p);
		}
		
		String date = rs.getString("report_date_mo");
		
		if (p.getOilProduction(date) == null) {
			p.add(new OilProduction(date,
						rs.getString("oil_pd_fore_mean"),
						rs.getString("oil_pd_fore_p10"),
						rs.getString("oil_pd_fore_p90"),
						"0.0"));
		}
		
		return p;
	}
}
