/**
 * SQL for accessing results.
 * 
 * @author  Michael Murray
 */
package ai.terra.sql.results;