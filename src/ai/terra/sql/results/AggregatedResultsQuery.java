package ai.terra.sql.results;

public abstract class AggregatedResultsQuery extends ResultsQuery {
	public abstract String getName();
}
