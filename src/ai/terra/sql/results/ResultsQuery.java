package ai.terra.sql.results;

import java.util.Collection;

import com.terra_ai.model.business.Group;
import com.terra_ai.model.forecast.OilProductionForecast;
import ai.terra.sql.PublicDataSourceSQL;

import io.almostrealism.persist.CascadingQuery;

public abstract class ResultsQuery extends CascadingQuery<PublicDataSourceSQL, Group, OilProductionForecast> {
	@Override
	public void init(Group key) { }
	
	@Override
	public Collection<OilProductionForecast> getReturnValue(Group key) {
		return key.getOilForecasts().values();
	}
	
	public boolean isRoot() { return true; }
}
