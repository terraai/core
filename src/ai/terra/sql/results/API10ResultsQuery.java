package ai.terra.sql.results;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import com.terra_ai.model.business.Group;
import com.terra_ai.model.forecast.OilProduction;
import com.terra_ai.model.forecast.OilProductionForecast;
import com.terra_ai.model.wells.API_10;
import com.terra_ai.model.wells.WellHead;

import io.almostrealism.persist.CascadingQuery;

public abstract class API10ResultsQuery extends AggregatedResultsQuery {
	@Override
	public String getQuery(Group g) {
		if (g.getAPIs().length() <= 0) return null;
		return "SELECT * from results.\"" + getName() + "\" where \"API_prod\" in (" + g.getAPIs() + ")";
	}
	
	@Override
	public OilProductionForecast process(ResultSet rs, Group g, Map<Class, List<CascadingQuery>> cascades) throws SQLException {
		WellHead h = g.getWell(new API_10(rs.getString("API_prod")));
		
		OilProductionForecast p = h.getOilForecasts().get(getName());
		if (p == null) {
			p = new OilProductionForecast(getName());
			h.getOilForecasts().put(getName(), p);
		}
		
		String date = rs.getString("report_date_mo");
		
		if (p.getOilProduction(date) == null) {
			p.add(new OilProduction(date,
						rs.getString("oil_pd_fore_mean"),
						rs.getString("oil_pd_fore_p10"),
						rs.getString("oil_pd_fore_p90"),
						"0.0"));
		}
		
		return p;
	}
}
