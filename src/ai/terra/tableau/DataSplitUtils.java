package ai.terra.tableau;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class DataSplitUtils {
	public static <T extends Comparable<? super T>> List<T> split(Collection<T> d, int slices, int index) {
		List<T> data = new ArrayList<T>();
		data.addAll(d);
		
		Collections.sort(data);
		int sliceSize = data.size() / slices;
		
		List<T> l = new ArrayList<T>();
		
		i: for (int i = sliceSize * index; i < (sliceSize * (index + 1)); i++) {
			if (i >= data.size()) break i;
			l.add(data.get(i));
		}
		
		return l;
	}
}
