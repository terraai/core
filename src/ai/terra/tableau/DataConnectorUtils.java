package ai.terra.tableau;

public class DataConnectorUtils {
	public static final boolean enableTableauExport = false;
	
	public static String createPage(String json) {
		if (!enableTableauExport) return json;
		
		StringBuffer buf = new StringBuffer();
		
		buf.append("<html>\n");
		buf.append("<meta http-equiv=\"Cache-Control\" content=\"no-store\" />\n");
		buf.append("<head>\n");
		buf.append("  <title>Terra AI</title>\n");
		buf.append("  <script src=\"https://online.tableau.com/javascripts/api/tableauwdc-1.1.0.js\" type=\"text/javascript\"></script>\n");
		buf.append("  <script src=\"http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js\" type=\"text/javascript\"></script>\n");
		buf.append("  <script type=\"text/javascript\">\n");
		buf.append("  (function() {\n");
		buf.append("      var myConnector = tableau.makeConnector();\n");
		
		buf.append("      myConnector.getColumnHeaders = function() {\n");
		buf.append("          var fieldNames = ['API_prod', 'state_API', 'report_date', 'oil', 'gas', 'water', 'prod_days'];\n");
		buf.append("          var fieldTypes = ['string', 'string', 'timestamp', 'string', 'string', 'string', 'string'];\n");
		buf.append("          tableau.headersCallback(fieldNames, fieldTypes);\n");
		buf.append("      }\n");

		buf.append("      myConnector.getTableData = function(lastRecordToken) {\n");
		buf.append("          var dataToReturn = " + json + ";\n");
		buf.append("          var lastRecordToken = 0;\n");
		buf.append("          var hasMoreData = false;\n");
		buf.append("          tableau.dataCallback(dataToReturn, lastRecordToken.toString(), hasMoreData);\n");
		buf.append("      }\n");

		buf.append("      tableau.registerConnector(myConnector);\n");
		buf.append("  })();\n");
		buf.append("$(document).ready(function() {\n");
		buf.append("$(\"#submitButton\").click(function() {\n");
		buf.append("        tableau.connectionName = \"Terra AI\" ;\n");
		buf.append("        tableau.connectionData = \"Data\";\n");
		buf.append("        tableau.submit();\n");
		buf.append("  })});\n");
		buf.append("  </script>\n");
		buf.append("</head>\n");
		buf.append("<body>\n");
		buf.append("<button type=\"button\" id=\"submitButton\">Get the Data</button>\n");
		buf.append("</body>\n");
		buf.append("</html>\n");
		
		return buf.toString();
	}
}
