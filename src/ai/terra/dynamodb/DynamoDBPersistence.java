package ai.terra.dynamodb;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import ai.terra.aws.AWSCredentials;
import com.terra_ai.model.BasicEntity;
import ai.terra.persistence.etl.TransformationMap;

/**
 * {@link DynamoDBPersistence} 
 * 
 * @author  Michael Murray
 */
public class DynamoDBPersistence implements TransformationMap {
	private static AmazonDynamoDB dynamoDB;

	public DynamoDBPersistence() {
		synchronized (DynamoDBPersistence.class) {
			if (dynamoDB == null) {
				AWSCredentialsProvider p = AWSCredentials.getProvider();
				System.out.println("Using AWS Access Key ID: " + p.getCredentials().getAWSAccessKeyId());
				dynamoDB = new AmazonDynamoDBClient(p).withRegion(Regions.US_WEST_2);
			}
		}
	}

	public void insert(BasicEntity entity) {
		DynamoDBMapper mapper = new DynamoDBMapper(dynamoDB);
		mapper.save(entity);
	}

	public BasicEntity retrieve(long id) {
		return null;
	}
}
